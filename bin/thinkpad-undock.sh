#!/bin/sh 

# Sauvegarde de l'emplacement des workspaces
python /home/me/config/bin/i3plug.py save

# Déplacement des workspaces vers l'écran du portable
i3-msg move workspace to output LVDS1

# 2 invocations because the Intel graphics card can only handle two outputs at a time
xrandr -d :0.0 --output LVDS1 --auto --output VGA1 --off

# On switch d'interface réseau
/sbin/ifdown eth0
/sbin/ifup wlan0

# on reload le wallpaper, au cas ou
feh --bg-scale /home/me/config/wallpaper/current.jpg
